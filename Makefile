CC=gcc

CFLAGS=-Wall -Wextra -Werror -O0 -g -std=c11 -I.. 

.PHONY: all clean


all: test

arvore.o: arvore.c

# coloque outras dependencias aqui


test: arvore.o



clean:
	rm -f *.o test

